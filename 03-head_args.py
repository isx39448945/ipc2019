#!/usr/bin/python3
# -*- coding:utf-8-*-
#03-head-args.py [-n nlin] [-f filein]
#  Mostar les n primeres línies (o 10)  de filein (o stdin).

# Definir els dos paràmetres opcionals i les variables on
# desar-los. Usar un str default de “/dev/stdin” com a nom 
# de fitxer per defecte, simplifica el codi, tenim sempre
# un string.

import sys, argparse
parser = argparse.ArgumentParser(description=\
        """Mostrar les N primereslínies """,\
        epilog="thats all folks")
parser.add_argument("-n","--nlin",type=int,\
        help="Número de línies",dest="nlin",\
        metavar="numLines",default=10)
parser.add_argument("-f","--fit",type=str,\
        help="fitxer a processar", metavar="file",\
        default="/dev/stdin",dest="fitxer")
args=parser.parse_args()
print(args)
# -------------------------------------------------------
MAXLIN=args.nlin
fileIn=open(args.fitxer,"r")
counter=0
for line in fileIn:
  counter+=1
  print(line, end=' ')
  if counter==MAXLIN: break
fileIn.close()
exit(0)
