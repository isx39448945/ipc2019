#!/usr/bin/python3
# -*- coding:utf-8-*-
#02-exemple-args.py
# *Argparser (Python documentation 14.5 / argparse tutorial):*
#  * creació objecte. afegir arguments.
#  * paràmetres posicionals i opcionals.
#  * help, description, epíleg, prog, metavar.
#  * destination, type.
#  * fer el parse. diccionari de resultats.
#  * fitxers de tipus file (inconvenients) o str.
#  * validació automàtica i missatges d’error. Independència de l’ordre dels 
#    arguments opcionals (no dels posicionals).
#  * validació automàtica del tipus dels arguments.
#  * Els *paràmetres posicionals* es defineixen per ordre i simplement amb el
#   “nom”. Els *opcionals* poden anar en qualsevol ordre i cal definir “-n” i “--nom”.


import argparse

parser=argparse.ArgumentParser(description="programa exemple de processar arguments",
prog="02-argument.py",epilog="hasta luego bro")
parser.add_argument("-e", "--edat", type=int, dest="useredat")
parser.add_argument("-f", "--fit", type=str, help="fitxer a processar", metavar="fileIn", dest="fitxer")
args=parser.parse_args()
print(parser)
print(args)
print(args.fitxer, args.useredat)
exit(0)
