#!/usr/bin/python3
# -*- coding:utf-8-*-
# 26-telnet-client.py -p port -s server

import sys,socket,argparse

parser = argparse.ArgumentParser(description="""telnet client""")
parser.add_argument("-s","--server",type=str,dest="server")
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()
HOST = args.server
PORT = args.port

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST,PORT))

while True:
	ordre = input("telnet> ")
	if not ordre: break
	s.sendall(ordre.encode("UTF-8"))      
	while True:
		data = s.recv(1024)
		data = data.decode("UTF-8")
		if data[-1] == chr(4):
			print(data[:-1])
			break
		print(data)
s.close()
sys.exit(0)
