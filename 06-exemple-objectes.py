#!/usr/bin/python3
# -*- coding:utf-8-*-
#06-exemple-objectes.py
#  Exemple de creació de una classe simplificada UnixUser amb camps login, uid, gid. 
# Constructor donats els tres valors, mètode show() i mètode sumaun()  que fa la tonteria
# de sumar 1 al uid.
# Crea objectes user1 i user2 de tipus UnixUser, els mostra, els posa a una llista.

class UnixUser():
	"""
	Classe UnixUser: prototipus de /etc/passwd
	login:passwd:uid:gid:gecos:home:shell
	"""
	def __init__(self,l,i,g):
		"Constructor objectes UnixUser"
		self.login=l
		self.uid=i
		self.gid=g
	def show(self):
		"Mostrar les dades de l'usuari"
		print("login:%s uid:%d gid:%d" % (self.login, self.uid, self.gid))
	def sumaun(self):
		"Suma 1 al uid"
		self.uid+=1
	def __str__(self):
		"Funció per retornar un string del objecte"
		return "%s %d %d" % (self.login, self.uid, self.gid)
		

# ~ >>> help(UnixUser)

# ~ >>> user1=15
# ~ >>> user1=UnixUser("marta",1000,100)
# ~ >>> user1
# ~ <__main__.UnixUser instance at 0x7f6384d9b170>
# ~ >>> user1.show()
# ~ login:marta uid:1000 gid:100
# ~ >>> user2=user1
# ~ >>> user2.show()
# ~ login:marta uid:1000 gid:100
# ~ >>> user2=UnixUser("anna",1001,100)
# ~ >>> user2.show()
# ~ login:anna uid:1001 gid:100
