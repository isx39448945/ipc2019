#!/usr/bin/python3
# -*- coding:utf-8-*-
# 22-daytime-server.py
# Crear un daytime client/server amb un popen de date. El server plega un cop respost. 
# En aquest cas el client pot ser qualsevol eina client que simplement es connecta i 
# escolta la resposta del servidor.
# El client es connecta al servidor  i aquest li retorna la data i tanca la conexió. El
# client mostra l adata rebuda i en veure que s'ha tancat la connexió també finalitza.
# El servidor engega i espera a rebre una conexió, quan l'accepta executa un Popen per
# fer un *date* del sistema operatiu, retorna la informació i tanca la conexió. També
# finalitza (és un servidor molt poc treballador!!).

import sys,socket
from subprocess import Popen, PIPE

HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# host i port per on escoltar al client
s.bind((HOST, PORT))

# escolta
s.listen(1)
conn, addr = s.accept()
print("Connected by", addr)

# fa el popen i envia al client la informació
ordre = ["date"]
pipeData = Popen(ordre,stdout=PIPE)
for line in pipeData.stdout:
	conn.send(line)

# tanca
conn.close()
sys.exit(0)

