#!/usr/bin/python3
# -*- coding:utf-8-*-
# 14-popen-sql-multi.py  -d database   [-c numclie]...
# Atacar la database indicada, llistar cada un dels registres del codi indicat.
# Explicacio dels diàlegs, un read o readlines es queda encallat at infinitum 
# quan el subprocess no finalitza. Cal llegir exactament les n linies de resposta
# (dues si ok, una si error), pero no infinites.
# Exemple amb wc < file i wc de stdin.
# *Atenció:* no programar el cas desfaborable de posar num_clie que no existeixen,
# això implica buscar un mecanisme de diàleg tipus “canvi!”... com amb els walki-talki.
#
# Hem après a:
# * posar el /n al final per que faci la acció.
# * si el popen finalitza és com si hi ha el ^d al final de fi de fluxe i podem llegir les n línies que retorni.
# * si el popem és interactiu i no finalitza cal saber a priori quantes línies llegir o es quedarà ‘enganxat’

import argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description=\
	"""Exemple popen sql multi""")
parser.add_argument("-d","--database",type=str,required=True,\
	help="database on ens volem connectar",metavar="database")
parser.add_argument("-c","--numclie",type=str,help="número de client",\
	metavar="numclie",dest="numclie",action="append")
args=parser.parse_args()

cmd = "psql -qtA -F',' -h 172.17.0.2 -U edtasixm06" + {args.database}
pipeData = Popen(cmd,shell=True,stdin=PIPE,stdout=PIPE,stderr=PIPE,\
	universal_newlines=True,bufsize=0)
pipeData.stdin.write("select * from clientes where numclie="+{args.numclie})

for line in pipeData.stdout:
	print(line.decode("utf-8"),end="")
exit(0)
