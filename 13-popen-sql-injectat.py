#!/usr/bin/python3
# -*- coding:utf-8-*-
# 13-popen-sql-injectat.py  consulta
# Fer que la sentència sql a fer sigui un argument tot entre cometes 
# que es passa com a argument.
# sql injectat: perills d’injectar codi d’usuari en els programes.

import argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description=\
	"""Exemple popen sql injectat""")
parser.add_argument("consulta",type=str,help="sentència sql")
args=parser.parse_args()

consulta = [f"psql -qtA -F',' -h 172.17.0.2 -U edtasixm06 training -c \"{args.consulta}\""]
pipeData = Popen(consulta,stdout=PIPE, shell=True)
for line in pipeData.stdout:
	print(line.decode("utf-8"),end="")
exit(0)

