#!/usr/bin/python3
# -*- coding:utf-8-*-
# 25-ps-client-one2one.py [-p port] server

import sys,socket,argparse,os
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description="""PS server""")
parser.add_argument("-p","--port",type=int, default=50001)
parser.add_argument("server",type=str)
args=parser.parse_args()
HOST = args.server
PORT = args.port

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
command = "ps ax"
pipeData = Popen(command,shell=True,stdout=PIPE)
for line in pipeData.stdout:
	s.send(line)
s.close()
