#!/usr/bin/python3
# -*- coding:utf-8-*-
# 15-exemple-signal.py
# Programa d'exemple del funcionament de signal. 
# Defineix handlers i els assigna als senyals 
# sigalarm i sigterm. Finalitza automàticament
# en passar els segons de l'alarma definida.

import sys,os,signal

def myhandler(signum,frame):
	print("Signal handler called with signal:", signum)
	print("hasta luego lucas!")
	sys.exit(1)


def mydeath(signum,frame):
	print("Signal handler called with signal:", signum)
	print("no em dona la gana de morir!")

signal.signal(signal.SIGALRM,myhandler) # señal 14
signal.signal(signal.SIGUSR2,myhandler) # señal 12
signal.signal(signal.SIGUSR1,mydeath) # señal 10
signal.signal(signal.SIGTERM,signal.SIG_IGN) # ignora la señal SIGTERM
signal.signal(signal.SIGINT,signal.SIG_IGN) # ignora la señal SIGINT
signal.alarm(60) # pasats 60 segs sense enviar cap señal , s'executa el SIGALRM
			     # que truca a la funcio myhandler equivalent a la señal 14
print(os.getpid())

while True:
	pass
signal.alarm(0) #inicialitza el signal.alarm a 0
sys.exit(0)
