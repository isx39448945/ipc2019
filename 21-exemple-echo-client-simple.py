#!/usr/bin/python3
# -*- coding:utf-8-*-
# 21-exemple-echo-server-simple.py*
# Fer els exemples de echo server/ echo client. 
# En l’exemple el client es hardcoded que escolta una resposta de mida fixa.
# Sockets: echo server/client bàsic on el client envia un text i el server el retorna. 
# Text acotat usant reciv encara no correctes del tot (encara no fem que tot 
# siguin bucles ni el caràcter de fi de diàleg).
# Fer primer el server i deixar-lo en marxa. Qualsevol client com ncat es pot comunicar amb el server.
# Observar el server amb netstat en mode listen i en mode established.

import sys,socket
HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

#s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
#s.connect((HOST, PORT))
#s.send(b'Hello, world')
#data = s.recv(1024)
#s.close()
#print('Received', repr(data))
#sys.exit(0)

s.bind((HOST,PORT))
s.listen(1) # es prepara per escoltar
conn, addr = s.accept() # es queda clavat fins que es produeixi una connexió
						# conn es un objecte del modul socket que fa referència
						# a la connexió
print("Connected by", addr)
while True:
	data = conn.recv(1024) # escolta 
	if not data: break # no diu: no hi ha més dades
					   # diu: s'ha tancat la connexió per l'altre extrem
	conn.send(data) # envia
conn.close()
sys.exit(0)

#[isx39448945@i18 ipc2019]$ ncat localhost 50001
