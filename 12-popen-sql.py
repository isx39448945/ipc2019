#!/usr/bin/python3
# -*- coding:utf-8-*-
# 12-popen-sql.py 
# Es pot fer una primera versió tot hardcoded sense diàleg amb: 
# psql -qtA -F';' training  -c “select * from oficinas;”.
# Executa la consulta “select * from oficinas;” usant psq.  
# Atenció: posar al popen shell=True.
# Podem usar un container Docker amb la bd training de postgres
# fent:
#
# Hi ha un docker a dockerhub:
# $ docker run --rm --name psql -h psql -it edtasixm06/postgres /bin/bash
# 
# A la adreça de github [asixm06-docker](https://github.com/edtasixm06/asixm06-docker/tree/master/postgres:base) hi la les ordres per engegar el postgres.
# 
# Cal fer-les per posar en marxa el servei i inicialitzar la base de dades.
# $ su -l postgres
# $ /usr/bin/pg_ctl -D /var/lib/pgsql/data -l logfile start
#
# Verificar el funcionament des de dins del container:
# $ psql -qtA -F',' training -c "select * from clientes;"
#
# Des del host executar consultes, cal indicar la adreça ip del container al que connectem, i l’usuari (role) que és edtasixm06:
# $  psql -qtA -F',' -h 172.17.0.2 -U edtasixm06 training -c "select * from clientes;"
#
# Si volem que el container faci map a un dels ports del host:
# $ docker run --rm --name psql -h psql -p 5432:5432  -it edtasixm06/postgres /bin/bash
# $inicialitzar des de dins del docker
# $ psql -qtA -F','  -h d02  -U edtasixm06 training -c "select * from oficinas;"

from subprocess import Popen, PIPE

consulta=["psql -qtA -F',' -h 172.17.0.2 -U edtasixm06 training -c  \"select * from clientes;\""]
pipeData = Popen(consulta,stdout=PIPE, shell=True)
for line in pipeData.stdout:
	print(line.decode("utf-8"),end="")
exit(0)

