#!/usr/bin/python3
# -*- coding:utf-8-*-
# 28-telnetServerMulti.py
# Implementar un servidor telnet amb connexions concurrents.

import sys,socket,argparse,select
from subprocess import Popen, PIPE

# Arguments
parser = argparse.ArgumentParser(description="""telnet server""")
parser.add_argument("-p","--port",type=int, default=50001)
parser.add_argument("-d","--debug",type=int, default=1)
args=parser.parse_args()
SENYAL = chr(4)
HOST = ''
PORT = args.port
DEBUG = args.debug

# Creació, bind i listen del socket verge
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
conns = [s]

while True:
	# Llista de connexions actives
	actius,x,y = select.select(conns,[],[])
	for actual in actius:
		# Si és una nova connexió
		if actual == s:
			conn, addr = s.accept()
			print ('Connected by', addr)
			conns.append(conn)
		# Si és una connexió ja establerta
		else:
			ordre = actual.recv(1024)
			# Si el client tanca la connexió
			if not ordre:
				sys.stdout.write("Client finalitzat: %s \n" % (actual))
				actual.close()
				conns.remove(actual)
			# Si el client demana informació
			else:
				pipeData = Popen(ordre,shell=True,stdout=PIPE,stderr=PIPE)
				for line in pipeData.stdout:
					line = line.decode("UTF-8")
					if DEBUG:
						sys.stderr.write(line)
					actual.sendall(line.encode("UTF-8"))
				for line in pipeData.stderr:
					line = line.decode("UTF-8")
					if DEBUG:
						sys.stderr.write(line)
					actual.sendall(line.encode("UTF-8"))
				actual.sendall(SENYAL.encode("UTF-8"))
s.close()
sys.exit(0)
			
        




