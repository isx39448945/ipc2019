#!/usr/bin/python3
# -*- coding:utf-8-*-
# 17-exemple-fork.py
# Exemple bàsic fork amb program apare que llança programa fill (un while infinit). 
# Observar els PID i la lògica del fluxe de funcionament.

import sys,os

print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid = os.fork()
if pid != 0:
	os.wait()
	print("Programa Pare", os.getpid(), pid)
else:
	print("Programa Fill", os.getpid(), pid)

print("Hasta luego!")
sys.exit(0)
