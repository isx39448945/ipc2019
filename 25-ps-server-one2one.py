#!/usr/bin/python3
# -*- coding:utf-8-*-
# 25-ps-server-one2one.py [-p port]
# Els clients es connecten a un servidor, envien un informe consistent en fer
# *"ps ax"* i finalitzen la connexió. El servidor rep l'informe del client i 
# el desa a disc. Cada informe es desa amb el format: ip-port-timestamt.log, on 
# timestamp té el format AADDMM-HHMMSS.
#
# Usar un servidor com el de l'exercici anterior, un daemon governat per senyals.

import sys,socket,os,signal,argparse, time

parser = argparse.ArgumentParser(description="""PS server""")
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()
llistaPeers = []
HOST = ''
PORT = args.port

def mysigusr1(signum,frame):
  print("Signal handler called with signal:", signum)
  print(llistaPeers)
  sys.exit(0)
  
def mysigusr2(signum,frame):
  print("Signal handler called with signal:", signum)
  print(len(llistaPeers))
  sys.exit(0)

def mysigterm(signum,frame):
  print("Signal handler called with signal:", signum)
  print(llistaPeers, len(llistaPeers))
  sys.exit(0)

pid=os.fork()
if pid !=0:
  print("Engegat el server CAL:", pid)
  sys.exit(0)

signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGUSR2,mysigusr2)
signal.signal(signal.SIGTERM,mysigterm)
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)

while True:
	conn, addr = s.accept()
	print("Connected by", addr)
	llistaPeers.append(addr)
	fitxer = "%s-%s-%s.log" % (addr[0], addr[1], time.strftime("%Y%d%m-%H%M%s"))
	f_fitxer = open(fitxer,"w")
	while True:
		data = conn.recv(1024)
		if not data: break
		f_fitxer.write(str(data))
	conn.close()
	f_fitxer.close()
