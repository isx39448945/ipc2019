#!/usr/bin/python3
# -*- coding:utf-8-*-
# 26-telnetServer.py [-p port] [-d debug]
# Implementar un servidor i un client telnet. Client i server fan un diàleg. 
# Cal un senyal de “yatà” Usem chr(4).
# Si s’indica debug el server genera per stdout la traça de cada connexió.

import sys,socket,argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description="""telnet server""")
parser.add_argument("-p","--port",type=int, default=50001)
parser.add_argument("-d","--debug",type=int, default=1)
args=parser.parse_args()
SENYAL = chr(4)
HOST = ''
PORT = args.port
DEBUG = args.debug

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)

while True:
	conn, addr = s.accept()
	print("Connected by", addr)
	while True:
		ordre = conn.recv(1024)
		if not ordre: break
		pipeData = Popen(ordre,shell=True,stdout=PIPE,stderr=PIPE)
		for line in pipeData.stdout:
			line = line.decode("UTF-8")
			if DEBUG:
				sys.stderr.write(line)
			conn.sendall(line.encode("UTF-8"))
		for line in pipeData.stderr:
			line = line.decode("UTF-8")
			if DEBUG:
				sys.stderr.write(line)
			conn.sendall(line.encode("UTF-8"))
		conn.sendall(SENYAL.encode("UTF-8"))
	conn.close()
s.close()
sys.exit(0)

		


